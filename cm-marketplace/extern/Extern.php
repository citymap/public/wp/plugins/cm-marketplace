<?php
namespace Citymap\Widget\Marketplace;
/**
 * The public-facing functionality of the plugin.
 *
 * @link       https://city-map.com
 * @since      1.0.0
 *
 * @package    Cm_Marketplace
 * @subpackage Cm_Marketplace/public
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the public-facing stylesheet and JavaScript.
 *
 * @package    Cm_Marketplace
 * @subpackage Cm_Marketplace/public
 * @author     Flexbone GmbH<info@flexbone.systems>
 */

class Extern {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $cm_marketplace    The ID of this plugin.
	 */
	private $cm_marketplace;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $cm_marketplace       The name of the plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $cm_marketplace, $version ) {

		$this->cm_marketplace = $cm_marketplace;
		$this->version = $version;

	}

	/**
	 * Register the stylesheets for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		wp_enqueue_style( $this->cm_marketplace, plugin_dir_url( __FILE__ ) . 'css/cm-marketplace-public.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		wp_enqueue_script( $this->cm_marketplace, plugin_dir_url( __FILE__ ) . 'js/cm-marketplace-public.js', array( 'jquery' ), $this->version, false );

	}

}
