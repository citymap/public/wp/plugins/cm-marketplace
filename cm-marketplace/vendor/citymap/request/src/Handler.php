<?php

namespace CityMap\Component\Request;

use CityMap\Authenticator\Authenticator;

class Handler{

    const REFERER_FALLBACK = 'city-map Request Handler';

    // Authenticator
    private $authenticator = null;

    private $follow_redirection = false;

    /*
        Configuration settings
    */
    private $settings = [];

    // Instance of CityMap\Authenticator
    public static $instance = null;

    private function __construct($settings){

        $this->settings = $settings;

        $this->authenticator = Authenticator::getInstance($settings);

    }

    /*
        Restricts the instantiation of this class to one "single" instance.

        array['api']                    array   Required parameters

                ['host']                string  URI entry point for all API requests

                ['public_key']          string  Public key of a city-map customer.
                                                This can be obtained by contacting your city-map partner.

                ['private_key']                 string  Private key of a city-map customer.
                                                This can be obtained by contacting your city-map partner.

                ['response_type']       string  At the moment, only JSON is supported

        array['handler']:               array   Optional parameters for CURL

                ['ssl_cipher_list']     string  Reccomended value: rsa_aes_256_sha, the option wont be set
                                                if this option is not provided

                ['ssl_verify_peer']     string  Defaults to false if not set

                ['ssl_verify_host'']    string  Defaults to false if not set


        @var  string[]  $settings   Settings requiered to correctly instatiate this class. (see above)

    */
    public static function getInstance(array $settings) {

        if (!isset(self::$instance)){

            self::$instance = new self($settings);

        }

        return self::$instance;

    }

    // Gets the error string and error code and returns them in an array
    private function getError($error, $error_no){

        return [
                'code' => $error_no,

                'message'=> $error

            ];

    }

    // Builds and executes the HTTP request using cURL
    private function httpRequest( $url, $http_method, $http_query_parameters ){

        $http_query_string = http_build_query($http_query_parameters);

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url);

        curl_setopt($ch, CURLOPT_FAILONERROR, true);

        curl_setopt($ch, CURLOPT_VERBOSE, true);

        curl_setopt($ch, CURLOPT_CERTINFO, true);

        // Set follow redirection
        if( isset($http_query_parameters['follow_redirection']) &&
            $http_query_parameters['follow_redirection'] != ''){

            $this->follow_redirection = $http_query_parameters['follow_redirection'];

        }

        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, $this->follow_redirection);

        switch($http_method){

            case 'POST':
            case 'SHOW':

                if($http_method == 'POST'){

                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

                }

                curl_setopt($ch, CURLOPT_FRESH_CONNECT, true);

                curl_setopt($ch, CURLOPT_POST, count($http_query_parameters));

                curl_setopt($ch, CURLOPT_POSTFIELDS, $http_query_string );

                $ssl_verify_peer = false;

                $ssl_verify_host = false;


                if( isset($this->settings['handler']['ssl_verify_peer']) &&
                    $this->settings['handler']['ssl_verify_peer'] != ''){

                    $ssl_verify_peer = $this->settings['handler']['ssl_verify_peer'];

                }

                if( isset($this->settings['handler']['ssl_verify_host']) &&
                    $this->settings['handler']['ssl_verify_host'] != ''){

                    $ssl_verify_host = $this->settings['handler']['ssl_verify_host'];

                }

                if( isset($this->settings['handler']['ssl_cipher_list']) &&
                    $this->settings['handler']['ssl_cipher_list'] != '' ){

                    curl_setopt($ch, CURLOPT_SSL_CIPHER_LIST, $this->settings['handler']['ssl_cipher_list']);

                }

                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, $ssl_verify_peer);

                curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, $ssl_verify_host);


            break;
            case 'DELETE':

                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'DELETE');

            break;
            case 'PUT':

                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');

                curl_setopt($ch, CURLOPT_POSTFIELDS, $http_query_string );

            break;

        }

        $response = curl_exec($ch);

        $error = curl_error($ch);

        $error_no = curl_errno ($ch);

        $http_status = (int)curl_getinfo($ch, CURLINFO_HTTP_CODE);

        curl_close($ch);

        unset($ch);

        if ($error) {

            $response = $this->getError($error, $error_no);

        }

        return $response;

    }

    private function getRequestQuery($api_action, $data){

        $http_query_parameters = [];

        // Signature for this http request
        $signature = $this->authenticator->getSignature($api_action);

        // Add data and the signed parameters to the http query string
        $http_query_parameters = array_merge($data, $this->authenticator->getSignedParameters());

        // Defalt referer
        $referer = self::REFERER_FALLBACK;

        // Check if host is set
        if(isset($_SERVER['HTTP_HOST']) && $_SERVER['HTTP_HOST'] != ''){

            $referer = $_SERVER['HTTP_HOST'];

        }

        // Add signature and referer
        return array_merge($http_query_parameters, [
                                                        'cmps_signature'    => $signature,

                                                        'script_uri'        => $referer,
                                                    ]);

    }

    private function request($http_method, $api_action, $data = []) {

        $url = $this->settings['api']['host']. $api_action.'/'. $this->settings['api']['response_type'].'/';

        // Make HTTP Request to the API host domain
        return $this->httpRequest( $url, $http_method, $this->getRequestQuery($api_action, $data) );

    }

    // Executes a GET request
    public function get($path, $params = [], $data = []) {

        return $this->request('GET', $path, $params, $data);

    }

    // Executes a SHOW request
    public function show($api_action, $data = []) {

        return $this->request('SHOW', $api_action, $data);

    }

    // Executes a POST request
    public function post($api_action, $data = []) {

        return $this->request('POST', $api_action, $data);

    }

    // Executes a DELETE request
    public function delete($api_action, $data = []) {

        return $this->request('DELETE', $api_action, $data);

    }

    // Executes a PUT request
    public function put($api_action, $data = []) {

        return $this->request('PUT', $api_action, $data);

    }

    // Redirection to another URL
    private function redirect($api_action, $data = []){

        $url = $this->settings['api']['host'].$api_action.'/'.$this->settings['api']['response_type'].'/?'.http_build_query($this->getRequestQuery($api_action, $data));

        header('Location: '.$url);

        die;

    }

}

?>
