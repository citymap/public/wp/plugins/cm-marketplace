<?php

/*
 * This file is part of the city-map SDK package.
 *
 * (c) Patricio Guerrero <info@flexbone.systems>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace CityMap\Authenticator;


/**
 * Authenticator allows generating valid signatures for requests made to the city-map API
 *
 * @author Patricio Guerrero <info@flexbone.systems>
 *
 * @final
 */
class Authenticator{

    private $signed_parameters = [];

    /*
        Configuration settings
    */
    private $settings = [];

    // Instance of CityMap\Authenticator
    public static $instance = null;

    private function __construct($settings){

        $this->settings = $settings;

    }

    /*
        Restricts the instantiation of this class to one "single" instance.

        array['api']
                    ['host']            string  URI entry point for all API requests

                    ['public_key']      string  Public key of a city-map customer.
                                                This can be obtained by contacting your city-map partner.

                    ['private_key']     string  Private key of a city-map customer.
                                                This can be obtained by contacting your city-map partner.

                    ['response_type']   string  At the moment, only JSON is supported


        @var  string[]  $settings   Settings requiered to correctly instatiate this class. (see above)

    */
    public static function getInstance(array $settings) {

        if (!isset(self::$instance)){

            self::$instance = new self($settings);

        }

        return self::$instance;

    }

    /*
        @return string Timestamp
    */
    private function getTimestamp(){

        $datetime = new \DateTime();

        $timezone = new \DateTimeZone('Europe/Berlin');

        $datetime->setTimezone($timezone);

        return $datetime->format('Y-m-d\TH:i:s\Z');

    }

    private function getStringToSign($api_action_name){

        $this->signed_parameters =  [   'cmps_action'        => $api_action_name,

                                        'cmps_response_type' => $this->settings['api']['response_type'],

                                        'cmps_api_public_key'    => $this->settings['api']['public_key'],

                                        'cmps_timestamp'     => $this->getTimestamp()

                                ];

        // We need the API host without protocol for this to work
        $host = preg_replace("(^https?://)", "", $this->settings['api']['host'] );

        return $api_action_name . "\n" . $host . "\n" . http_build_query($this->signed_parameters);

    }

    public function getSignature($api_action){

        // create the string to sign
        $string_to_sign = $this->getStringToSign($api_action);

        // calculate HMAC with SHA256 and base64-encoding
        $valid_signature = base64_encode(hash_hmac('sha256', $string_to_sign, $this->settings['api']['private_key'], TRUE));

        return $valid_signature;

    }

    // Returns the signed parameters array
    public function getSignedParameters(){

        return $this->signed_parameters;

    }


}


?>
