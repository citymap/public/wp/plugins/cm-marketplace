<?php

/*
 *
 * @link              https://city-map.com
 * @since             1.0.0
 * @package           Cm_Business_Directory
 *
 * @wordpress-plugin
 * Plugin Name:       city-map Business Directory
 * Plugin URI:        https://city-map.com/
 * Description:       city-map business directory plugin for displaying business cards within an Elementor WP site.
 * Version:           3.5.2
 * Author:            Flexbone GmbH
 * Author URI:        https://flexbone.systems
 * License:           MIT
 * Text Domain:       cm-marketplace
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if (!defined('WPINC')) {
	die;
}

require 'vendor/autoload.php';

use Citymap\Widget\Marketplace;

use Citymap\Widget\Marketplace\Activator;

use Citymap\Widget\Marketplace\Deactivator;

/**
 * Currently plugin version.
 */
define('CM_MARKETPLACE_VERSION', '3.5.2');

define('CM_MARKETPLACE_ELEMENTOR', plugin_dir_url(__FILE__));

define('CM_MARKETPLACE_ELEMENTOR_VAR_PREFIX', '__cm__');

define('CM_MARKETPLACE_PLUGIN_DIR', plugin_dir_url(__FILE__));

/**
 * The code that runs during plugin activation.
 * This action is documented in lib/activator.php
 */
function activate_cm_marketplace()
{

	require_once plugin_dir_path(__FILE__) . 'lib/Activator.php';

	Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in lib/deactivator.php
 */
function deactivate_cm_marketplace()
{

	require_once plugin_dir_path(__FILE__) . 'lib/Deactivator.php';

	Deactivator::deactivate();
}

// Initialize Elementor Add On
function register_elementor_widget()
{

	// Load plugin file
	require_once(__DIR__ . '/lib/Elementor/Ready.php');

	// Run the plugin
	\Citymap\Widget\Marketplace\Elementor\Ready::getInstance();
}

register_activation_hook(__FILE__, 'activate_cm_marketplace');

register_deactivation_hook(__FILE__, 'deactivate_cm_marketplace');

add_action('plugins_loaded', 'register_elementor_widget');

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path(__FILE__) . 'lib/Marketplace.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */

function run_cm_marketplace()
{

	$plugin = new Marketplace();

	$plugin->run();
}

run_cm_marketplace();
