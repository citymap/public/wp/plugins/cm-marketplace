<?php
namespace Citymap\Widget\Marketplace;
/**
 * Fired during plugin activation
 *
 * @link       https://city-map.com
 * @since      1.0.0
 *
 * @package    Cm_Marketplace
 * @subpackage Cm_Marketplace/lib
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Cm_Marketplace
 * @subpackage Cm_Marketplace/lib
 * @author     Flexbone GmbH<info@flexbone.systems>
 */

use Citymap\Widget\Marketplace\Admin;

class Activator {

	/**
	 * Activates the cm-marketplace plugin.
	 *
	 * Activates the cm-marketplace plugin and redirects the visitor to
	 * the API keys input mask.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {

		add_option(Admin::ACTIVE_PLUGIN, true );

	}

}
