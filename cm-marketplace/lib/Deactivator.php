<?php

namespace Citymap\Widget\Marketplace;

/**
 * Fired during plugin deactivation
 *
 * @link       https://city-map.com
 * @since      1.0.0
 *
 * @package    Cm_Marketplace
 * @subpackage Cm_Marketplace/lib
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Cm_Marketplace
 * @subpackage Cm_Marketplace/lib
 * @author     Flexbone GmbH<info@flexbone.systems>
 */

use Citymap\Widget\Marketplace\Admin;

class Deactivator
{

	/**
	 * Removes the cm-marketplace plugin.
	 *
	 * Removes the cm-marketplace plugin and deletes both API keys from Wordpress.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate()
	{

		// delete product activation token
		// delete_option(Admin::PRODUCT_ACTIVATION_TOKEN);

		// delete empty submenu page
		remove_submenu_page('city-map', 'city-map');

		// delete plugin submenu page
		remove_submenu_page('city-map', Admin::SETTINGS_URL);
	}
}
