<?php
namespace Citymap\Widget;

/**
 * The file that defines the core plugin class
 *
 * A class definition that includes attributes and functions used across both the
 * public-facing side of the site and the admin area.
 *
 * @link       https://city-map.com
 * @since      1.0.0
 *
 * @package    Citymap\Widget\Marketplace
 * @subpackage Citymap\Widget\Marketplace/lib
 */

/**
 * The core plugin class.
 *
 * This is used to define internationalization, admin-specific hooks, and
 * public-facing site hooks.
 *
 * Also maintains the unique identifier of this plugin as well as the current
 * version of the plugin.
 *
 * @since      1.0.0
 * @package     Citymap\Widget\Marketplace
 * @subpackage  Citymap\Widget\Marketplace/lib
 * @author      Flexbone GmbH<info@flexbone.systems>
 */

use Citymap\Widget\Marketplace\Loader;

use Citymap\Widget\Marketplace\Admin;

use Citymap\Widget\Marketplace\Extern;

use Citymap\Widget\Marketplace\i18n;

class Marketplace {

	/**
	 * The loader that's responsible for maintaining and registering all hooks that power
	 * the plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      Citymap\Widget\Marketplace\Loader    $loader    Maintains and registers all hooks for the plugin.
	 */
	protected $loader;

	/**
	 * The unique identifier of this plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string    $cm_marketplace    The string used to uniquely identify this plugin.
	 */
	protected $cm_marketplace;

	/**
	 * The current version of the plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string    $version    The current version of the plugin.
	 */
	protected $version;

	/**
	 * Define the core functionality of the plugin.
	 *
	 * Set the plugin name and the plugin version that can be used throughout the plugin.
	 * Load the dependencies, define the locale, and set the hooks for the admin area and
	 * the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function __construct() {

		if ( defined( 'CM_MARKETPLACE_VERSION' ) ) {

			$this->version = CM_MARKETPLACE_VERSION;

		} else {

			$this->version = '1.0.0';

		}

		$this->cm_marketplace = 'cm-marketplace';

		$this->load_dependencies();

		$this->set_locale();

		$this->define_admin_hooks();

		$this->define_public_hooks();

	}

	/**
	 * Load the required dependencies for this plugin.
	 *
	 * Include the following files that make up the plugin:
	 *
	 * - Citymap\Widget\Marketplace\Loader. Orchestrates the hooks of the plugin.
	 * - Cm_Marketplace_i18n. Defines internationalization functionality.
	 * - Citymap\Widget\Marketplace\Admin. Defines all hooks for the admin area.
	 * - Cm_Marketplace_Public. Defines all hooks for the public side of the site.
	 *
	 * Create an instance of the loader which will be used to register the hooks
	 * with WordPress.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function load_dependencies() {

		/**
		 * The class responsible for orchestrating the actions and filters of the
		 * core plugin.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'lib/Loader.php';

		/**
		 * The class responsible for defining internationalization functionality
		 * of the plugin.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'lib/I18n.php';

		/**
		 * The class responsible for defining all actions that occur in the admin area.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/Admin.php';

		/**
		 * The class responsible for defining all actions that occur in the public-facing
		 * side of the site.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'extern/Extern.php';


		// Check for Elementor
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'lib/Elementor/Ready.php';


		$this->loader = new Loader();

	}

	/**
	 * Define the locale for this plugin for internationalization.
	 *
	 * Uses the Cm_Marketplace_i18n class in order to set the domain and to register the hook
	 * with WordPress.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function set_locale() {

		$plugin_i18n = new i18n();

		$this->loader->add_action( 'plugins_loaded', $plugin_i18n, 'load_plugin_textdomain' );

	}

	/**
	 * Register all of the hooks related to the admin area functionality
	 * of the plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function define_admin_hooks() {

		$plugin_admin = new Admin( $this->get_cm_marketplace(), $this->get_version() );

		$this->loader->add_action( 'admin_init', $plugin_admin, 'init' );

		$this->loader->add_action( 'admin_init', $plugin_admin, 'registerSettingsOptions' );

		$this->loader->add_action( 'admin_notices', $plugin_admin, 'hasValidApiKey');

		$this->loader->add_action( 'admin_menu', $plugin_admin, 'registerSettingsPage' );

        $this->loader->add_action( 'admin_menu', $plugin_admin, 'addPluginSettingsPage' );

		$this->loader->add_action( 'admin_menu', $plugin_admin, 'firstSubmenuOverride' );

		$this->loader->add_action( 'admin_enqueue_scripts', $plugin_admin, 'enqueue_styles' );

		$this->loader->add_action( 'admin_enqueue_scripts', $plugin_admin, 'enqueue_scripts' );

    //    $this->loader->add_filter( 'menu_order','', '__return_true' );

    //    $this->loader->add_filter( 'custom_menu_order', '', '__return_true' );


	}

	/**
	 * Register all of the hooks related to the public-facing functionality
	 * of the plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function define_public_hooks() {

		$plugin_public = new Extern( $this->get_cm_marketplace(), $this->get_version() );

		$this->loader->add_action( 'wp_enqueue_scripts', $plugin_public, 'enqueue_styles' );

		$this->loader->add_action( 'wp_enqueue_scripts', $plugin_public, 'enqueue_scripts' );

	}

	/**
	 * Run the loader to execute all of the hooks with WordPress.
	 *
	 * @since    1.0.0
	 */
	public function run() {
		$this->loader->run();
	}

	/**
	 * The name of the plugin used to uniquely identify it within the context of
	 * WordPress and to define internationalization functionality.
	 *
	 * @since     1.0.0
	 * @return    string    The name of the plugin.
	 */
	public function get_cm_marketplace() {
		return $this->cm_marketplace;
	}

	/**
	 * The reference to the class that orchestrates the hooks with the plugin.
	 *
	 * @since     1.0.0
	 * @return    Citymap\Widget\Marketplace\Loader    Orchestrates the hooks of the plugin.
	 */
	public function get_loader() {
		return $this->loader;
	}

	/**
	 * Retrieve the version number of the plugin.
	 *
	 * @since     1.0.0
	 * @return    string    The version number of the plugin.
	 */
	public function get_version() {
		return $this->version;
	}

}
