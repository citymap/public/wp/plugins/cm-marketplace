<?php

$this->start_controls_section(

    'business_directory',
    [
        'label' => esc_html__( 'Business Directory', 'cm-marketplace' ),

        'tab' => \Elementor\Controls_Manager::TAB_CONTENT,

    ]

);

    // Input for radius, depends on query method
    $this->add_control(

        CM_MARKETPLACE_ELEMENTOR_VAR_PREFIX.'customer_list_use_category',
        [

            'type' => \Elementor\Controls_Manager::TEXT,

            'label' => esc_html__( 'Industry Sector', 'cm-marketplace' ),

            'placeholder' => esc_html__( 'Hotels, Restarurants, Lawyers, etc.', 'cm-marketplace' ),

        ]

    );
    $this->add_control(

        CM_MARKETPLACE_ELEMENTOR_VAR_PREFIX.'by_association',
        [
            'type' => \Elementor\Controls_Manager::SELECT,

            'label' => esc_html__( 'Filter by Organization', 'cm-marketplace' ),

            'options' => [

                'default' => esc_html__( 'Select one', 'cm-marketplace' ),

                'no' => esc_html__( 'No', 'cm-marketplace' ),

                'yes' => esc_html__( 'Yes', 'cm-marketplace' ),

            ],

            'default' => 'no',

        ]
    );

    $this->add_control(

        CM_MARKETPLACE_ELEMENTOR_VAR_PREFIX.'organization_id',
        [

            'type' => \Elementor\Controls_Manager::TEXT,

            'label' => esc_html__( 'Organization', 'cm-marketplace' ),

            'placeholder' => esc_html__( 'Enter organization ID', 'cm-marketplace' ),

            'condition' => [

                CM_MARKETPLACE_ELEMENTOR_VAR_PREFIX.'by_association' => 'yes'

            ],

        ]

    );

$this->end_controls_section();
