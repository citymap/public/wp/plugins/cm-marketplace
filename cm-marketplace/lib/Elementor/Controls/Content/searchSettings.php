<?php

$this->start_controls_section(

    'settings',
    [
        'label' => esc_html__( 'Search', 'cm-marketplace' ),

        'tab' => \Elementor\Controls_Manager::TAB_CONTENT,

    ]

);

    // Method selector for location based queries
    $this->add_control(

        CM_MARKETPLACE_ELEMENTOR_VAR_PREFIX.'query_method',
        [
            'type' => \Elementor\Controls_Manager::SELECT,

            'label' => esc_html__( 'Method', 'cm-marketplace' ),

            'options' => [

                'default' => esc_html__( 'Select one', 'cm-marketplace' ),

                'radius' => esc_html__( 'Distance', 'cm-marketplace' ),

                'geo_polygon' => esc_html__( 'Geo polygon', 'cm-marketplace' ),

            ],

            'default' => 'radius',

        ]
    );

    // Input for radius, depends on query method
    $this->add_control(

        CM_MARKETPLACE_ELEMENTOR_VAR_PREFIX.'customer_list_location',
        [

            'type' => \Elementor\Controls_Manager::TEXT,

            'label' => esc_html__( 'Location', 'cm-marketplace' ),

            'placeholder' => esc_html__( 'Town / State / Coordinates', 'cm-marketplace' ),

            'condition' => [

                CM_MARKETPLACE_ELEMENTOR_VAR_PREFIX.'query_method' => 'radius'

            ],

        ]

    );

    // Input for distance, depends on query method
    $this->add_control(

        CM_MARKETPLACE_ELEMENTOR_VAR_PREFIX.'radius_value',
        [

            'type' => \Elementor\Controls_Manager::TEXT,

            'label' => esc_html__( 'Distance', 'cm-marketplace' ),

            'placeholder' => esc_html__( 'Enter radius in Km', 'cm-marketplace' ),

            'condition' => [

                CM_MARKETPLACE_ELEMENTOR_VAR_PREFIX.'query_method' => 'radius'

            ],

        ]

    );

    // Radius units
    $this->add_control(

        CM_MARKETPLACE_ELEMENTOR_VAR_PREFIX.'distance_units',
        [

            'type' => \Elementor\Controls_Manager::SELECT,

            'label' => esc_html__( 'Units', 'cm-marketplace' ),

            'options' => [

                'default' => esc_html__( 'Select one', 'cm-marketplace' ),

                'km' => esc_html__( 'Kilometers', 'cm-marketplace' ),

                'mi' => esc_html__( 'Miles', 'cm-marketplace' ),

                'meters' => esc_html__( 'Meters', 'cm-marketplace' ),

            ],

            'default' => 'km',

            'condition' => [

                CM_MARKETPLACE_ELEMENTOR_VAR_PREFIX.'query_method' => 'radius'

            ],

        ]

    );

    // Input for geo polygon, depends on query method
    $this->add_control(

        CM_MARKETPLACE_ELEMENTOR_VAR_PREFIX.'geo_polygon_url',
        [

            'type' => \Elementor\Controls_Manager::TEXT,

            'label' => esc_html__( 'Geo polygon', 'cm-marketplace' ),

            'placeholder' => esc_html__( 'Enter URL of polygon file', 'cm-marketplace' ),

            'condition' => [

                CM_MARKETPLACE_ELEMENTOR_VAR_PREFIX.'query_method' => 'geo_polygon'

            ],

        ]

    );


    // Input for radius, depends on query method
    $this->add_control(

        CM_MARKETPLACE_ELEMENTOR_VAR_PREFIX.'max_per_page',
        [

            'type' => \Elementor\Controls_Manager::TEXT,

            'label' => esc_html__( 'Entries per page', 'cm-marketplace' ),

            'placeholder' => esc_html__( 'Maximum entries per page' ),

            'default' => 20

        ]

    );


$this->end_controls_section();
