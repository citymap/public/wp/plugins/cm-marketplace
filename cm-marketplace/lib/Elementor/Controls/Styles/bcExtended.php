<?php

// businessCARD optical settings

$this->start_controls_section(

    'bc_extended',
    [
        'label' => esc_html__( 'businessCARD', 'cm-marketplace' ),

        'tab' => \Elementor\Controls_Manager::TAB_STYLE,

    ]

);

    $this->add_control(

        CM_MARKETPLACE_ELEMENTOR_VAR_PREFIX.'more_info_button',
        [
            'type' => \Elementor\Controls_Manager::SELECT,

            'label' => esc_html__( 'More Info Button', 'cm-marketplace' ),

            'options' => [

                'default' => esc_html__( 'Select one', 'cm-marketplace' ),

                'yes' => esc_html__( 'Yes', 'cm-marketplace' ),

                'no' => esc_html__( 'No', 'cm-marketplace' ),

            ],

            'default' => 'yes',

        ]
    );

    $this->add_responsive_control(

		CM_MARKETPLACE_ELEMENTOR_VAR_PREFIX.'bc_width',
		[
			'label' => esc_html__( 'Extended card width', 'plugin-name' ),
			'type' => \Elementor\Controls_Manager::SLIDER,
			'size_units' => [ 'px', '%' ],
			'range' => [
				'px' => [
					'min' => 500,
					'max' => 2000,
					'step' => 5,
				],
				'%' => [
					'min' => 50,
					'max' => 100,
				],
			],
			'default' => [
				'unit' => '%',
				'size' => 100,
			],
			'selectors' => [
				'{{WRAPPER}} .card.cmbc-extended' => 'width: calc({{SIZE}}{{UNIT}} - 8px) !important;',
			],
		]
	);

    $this->add_responsive_control(

		CM_MARKETPLACE_ELEMENTOR_VAR_PREFIX.'bc_height',
		[
			'label' => esc_html__( 'Extended card height (PX)', 'plugin-name' ),
			'type' => \Elementor\Controls_Manager::SLIDER,
			'size_units' => [ 'px' ],
			'range' => [
				'px' => [
					'min' => 250,
					'max' => 600,
					'step' =>2,
				],
			],

            'devices' => [ 'desktop', 'tablet', 'mobile' ],

			'desktop_default' => [
				'size' => 250,
				'unit' => 'px',
			],

			'tablet_default' => [
				'size' => 250,
				'unit' => 'px',
			],

			'mobile_default' => [
				'size' => 'auto',
				'unit' => '',
			],

			'selectors' => [
				'{{WRAPPER}} .card.cmbc-extended' => 'height: {{SIZE}}{{UNIT}} !important;',
			],
		]
	);

    $this->add_responsive_control(

		CM_MARKETPLACE_ELEMENTOR_VAR_PREFIX.'bc_content_height',
		[
			'label' => esc_html__( 'Short info height (PX)', 'plugin-name' ),
			'type' => \Elementor\Controls_Manager::SLIDER,
			'size_units' => [ 'px' ],
			'range' => [
				'px' => [
					'min' => 100,
					'max' => 600,
					'step' =>2,
				],
			],
			'default' => [
				'unit' => 'px',
				'size' => 95,
			],
			'selectors' => [
				'{{WRAPPER}} .cmbc-company-content' => 'height: {{SIZE}}{{UNIT}} !important;',
			],
		]
	);

$this->end_controls_section();
