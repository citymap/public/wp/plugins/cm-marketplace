<?php

$this->start_controls_section(

    'bc_typography',
    [
        'label' => esc_html__( 'Typography', 'cm-marketplace' ),

        'tab' => \Elementor\Controls_Manager::TAB_STYLE,

    ]

);
    $this->add_control(

        CM_MARKETPLACE_ELEMENTOR_VAR_PREFIX.'icons_heading_color',
        [
            'label' => esc_html__( 'Icons and heading color', 'cm-marketplace' ),

            'type' => \Elementor\Controls_Manager::COLOR,

            'selectors' => [

                '{{WRAPPER}} .cmbc-colored' => 'color: {{VALUE}} !important',

                '{{WRAPPER}} .pagination-link.active-item' => 'background-color: {{VALUE}} !important',

            ],
        ]
    );

    $this->add_group_control(

        \Elementor\Group_Control_Typography::get_type(),

        [
            'label' => esc_html__( 'Company name', 'cm-marketplace' ),

            'name' => CM_MARKETPLACE_ELEMENTOR_VAR_PREFIX.'card_title_typography',

            'selector' => '{{WRAPPER}} .cmbc-company-name',
        ]

    );

    $this->add_group_control(

        \Elementor\Group_Control_Typography::get_type(),

        [
            'label' => esc_html__( 'Subtitle', 'cm-marketplace' ),

            'name' => CM_MARKETPLACE_ELEMENTOR_VAR_PREFIX.'subtitle_typography',

            'selector' => '{{WRAPPER}} .cmbc-subheadline',
        ]

    );

    $this->add_group_control(

        \Elementor\Group_Control_Typography::get_type(),

        [
            'label' => esc_html__( 'Card content', 'cm-marketplace' ),

            'name' => CM_MARKETPLACE_ELEMENTOR_VAR_PREFIX.'card_content_typography',

            'selector' => '{{WRAPPER}} .cmbc-company-content',
        ]

    );

    $this->add_group_control(

        \Elementor\Group_Control_Typography::get_type(),

        [
            'label' => esc_html__( 'Address items', 'cm-marketplace' ),

            'name' => CM_MARKETPLACE_ELEMENTOR_VAR_PREFIX.'address_items_typography',

            'selector' => '{{WRAPPER}} .cmbc-address-item',
        ]

    );

    $this->add_group_control(

        \Elementor\Group_Control_Typography::get_type(),

        [
            'label' => esc_html__( 'Industry name', 'cm-marketplace' ),

            'name' => CM_MARKETPLACE_ELEMENTOR_VAR_PREFIX.'industry_name_typography',

            'selector' => '{{WRAPPER}} .cmbc-industry',
        ]

    );


$this->end_controls_section();
