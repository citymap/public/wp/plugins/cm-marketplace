<?php

// companyCARD optical settings

$this->start_controls_section(

    'bc_small',
    [
        'label' => esc_html__( 'companyCARD', 'cm-marketplace' ),

        'tab' => \Elementor\Controls_Manager::TAB_STYLE,

    ]

);

    $this->add_responsive_control(

		CM_MARKETPLACE_ELEMENTOR_VAR_PREFIX.'bc_small_width',
		[
			'label' => esc_html__( 'companyCARD width', 'plugin-name' ),
			'type' => \Elementor\Controls_Manager::SLIDER,
			'size_units' => [ 'px', '%' ],
			'range' => [
				'px' => [
					'min' => 100,
					'max' => 250,
					'step' => 5,
				],
				'%' => [
					'min' => 0,
					'max' => 100,
				],
			],
			'default' => [
				'unit' => '%',
				'size' => 50,
			],

            'devices' => [ 'desktop', 'tablet', 'mobile' ],

			'desktop_default' => [
				'size' => 50,
				'unit' => '%',
			],

			'tablet_default' => [
				'size' => 50,
				'unit' => '%',
			],

			'mobile_default' => [
				'size' => 100,
				'unit' => '%',
			],

			'selectors' => [
				'{{WRAPPER}} .card.cmbc-small' => 'width: calc({{SIZE}}{{UNIT}} - 8px) !important;',
			],
		]
	);

    $this->add_responsive_control(

		CM_MARKETPLACE_ELEMENTOR_VAR_PREFIX.'bc_small_height_px',
		[
			'label' => esc_html__( 'companyCARD height (PX)', 'plugin-name' ),
			'type' => \Elementor\Controls_Manager::SLIDER,
			'size_units' => [ 'px' ],
			'range' => [
				'px' => [
					'min' => 180,
					'max' => 600,
					'step' => 5,
				],
			],

			'default' => [
				'unit' => 'px',
				'size' => 180,
			],

			'selectors' => [
				'{{WRAPPER}} .card.cmbc-small' => 'height: {{SIZE}}px !important;',
			],
		]
	);

$this->end_controls_section();
