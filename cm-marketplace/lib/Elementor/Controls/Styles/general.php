<?php

// businessCARD optical settings

$this->start_controls_section(

    'bc_general',
    [
        'label' => esc_html__( 'General', 'cm-marketplace' ),

        'tab' => \Elementor\Controls_Manager::TAB_STYLE,

    ]

);

    $this->add_responsive_control(

		CM_MARKETPLACE_ELEMENTOR_VAR_PREFIX.'list_width',
		[
			'label' => esc_html__( 'List Width', 'plugin-name' ),
			'type' => \Elementor\Controls_Manager::SLIDER,
			'size_units' => [ 'px', '%' ],
			'range' => [
				'px' => [
					'min' => 0,
					'max' => 3000,
					'step' => 3,
				],
				'%' => [
					'min' => 0,
					'max' => 100,
				],
			],
			'default' => [
				'unit' => 'px',
				'size' => 800,
			],
			'selectors' => [
				'{{WRAPPER}} .cmcom-customer-list' => 'width: {{SIZE}}{{UNIT}};',
			],
		]
	);

    $this->add_control(

        CM_MARKETPLACE_ELEMENTOR_VAR_PREFIX.'use_default_styles',
        [
            'type' => \Elementor\Controls_Manager::SELECT,

            'label' => esc_html__( 'Add pagination on top', 'cm-marketplace' ),

            'options' => [

                'default' => esc_html__( 'Select one', 'cm-marketplace' ),

                'yes' => esc_html__( 'Yes', 'cm-marketplace' ),

                'no' => esc_html__( 'No', 'cm-marketplace' ),

            ],

            'default' => 'no',

        ]
    );

$this->end_controls_section();
