<?php
namespace Citymap\Widget\Marketplace\Elementor;

if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly.
}

use \Elementor\Widget_Base;

use CityMap\Component\Request\Handler;

use Citymap\Widget\Marketplace\Admin;

use Citymap\Widget\Marketplace\Elementor\Styles;

use Elementor\Group_Control_Typography;

use Elementor\Scheme_Typography;

class AddOn extends \Elementor\Widget_Base {

    public function get_name() {

        return __( 'city-map Business Directory', 'cm-marketplace' );

    }

    public function get_title() {

        return __( 'city-map Business Directory', 'cm-marketplace' );

    }

    public function get_icon() {

        return 'eicon-section';

    }

    public function get_categories() {

        return [ 'city-map' ];

    }

    public function get_script_depends() {

        wp_register_script('fontawesome','https://kit.fontawesome.com/3ed602679d.js');
        wp_register_script('bootstrap-5','https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js');

        return [
            'fontawesome',
            'bootstrap-5',
        ];

    }

    public function get_style_depends() {

        wp_register_style(  "cm-business-card",
                            "https://city-map.com/static/branding/cmcom_wp/css/wp-business-card.css",
                            [],
                            false, "all" );

        wp_register_style(  "bootstrap-5",
                            "https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css",
                            [],
                            false, "all" );

        return [

            'cm-business-card',
            'bootstrap-5',

        ];
    }

    protected function register_controls() {

        // Controls for industry and organizations
        require_once plugin_dir_path( dirname( __FILE__ ) ) . 'Elementor/Controls/Content/businessDirectory.php';

        // Conrols for geographical queries
        require_once plugin_dir_path( dirname( __FILE__ ) ) . 'Elementor/Controls/Content/searchSettings.php';

        // General settings style
        require_once plugin_dir_path( dirname( __FILE__ ) ) . 'Elementor/Controls/Styles/general.php';

        // businessCARD Settings
        require_once plugin_dir_path( dirname( __FILE__ ) ) . 'Elementor/Controls/Styles/bcExtended.php';

        // companyCARD Settings
        require_once plugin_dir_path( dirname( __FILE__ ) ) . 'Elementor/Controls/Styles/bcSmall.php';

        // Typography
        require_once plugin_dir_path( dirname( __FILE__ ) ) . 'Elementor/Controls/Styles/typography.php';

    }

    protected function render() {

        if(!Admin::hasValidProductToken()){

            echo '
                    <span class="badge bg-danger d-flex align-items-center py-2 px-3 rounded-pill" role="alert">
                    <i class="fa-solid fa-skull me-3 fa-shake fs-5"></i>'.esc_html__('Please activate your plugin with a valid token before using it!', 'cm-marketplace').'</span>';

            return;

        }

        $plugin_settings = $this->get_settings_for_display();

        $credentials = [

            'api' => [

                'response_type' => 'html',

                'host' => 'https://my.cmpowersite.com/api/',

                'public_key' => get_option(Admin::PUBLIC_API_KEY_OPTION),

                'private_key' => get_option(Admin::PRIVATE_API_KEY_OPTION),

            ]

        ];

        $request_handler = Handler::getInstance($credentials);

        if($plugin_settings['__cm__more_info_button'] == 'yes'){

            $plugin_settings['__cm__more_info_button'] = true;

        }else{

            $plugin_settings['__cm__more_info_button'] = false;

        }


        $paginator_slug = '__cm_page';

        $page = 1;


        // get page number
        if(!empty($_GET[$paginator_slug]) && $_GET[$paginator_slug] != ''){

            $page = $_GET[$paginator_slug];

        }

        $redirect_url = '';

        // If REDIRECT_URL is set, the site is probably using pretty URLs
        // we need to add this string before the paginator slug later on
        if(isset($_SERVER['REDIRECT_URL']) && $_SERVER['REDIRECT_URL'] != ''){

            $redirect_url = $_SERVER['REDIRECT_URL'];

        }

        // We have some vars in the URL
        if(!empty($_GET)){

            unset($_GET[$paginator_slug]);

            $vars = $_GET;

            $paginator_slug = $redirect_url.'?'.http_build_query($_GET).'&'.$paginator_slug;

        }else{

            $paginator_slug = $redirect_url.'?'.$paginator_slug;

        }


        $search = [

            'method' => 'cmcom.lists.getBusinessListings',

            'parameters' => [
                'host' => 'wp.city-map.com',
                'use_default_styles' => $plugin_settings['__cm__use_default_styles'],
                'customer_list_type' => 'branchenbuch',
                'radius' => $plugin_settings['__cm__radius_value'],
                'distance_units' => $plugin_settings['__cm__distance_units'],
                'max_per_page' => $plugin_settings['__cm__max_per_page'],
                'sorting' => 'desc',
                'more_info_button' => $plugin_settings['__cm__more_info_button'],
                'custom_business_cards' => 'no',
                'organization_id' => $plugin_settings['__cm__organization_id'],
                'language_code' => 'de',
                'customer_list_location' => $plugin_settings['__cm__customer_list_location'],
                'customer_list_use_category' => $plugin_settings['__cm__customer_list_use_category'],
                'page' => $page,
                'paginator_slug' => $paginator_slug,
            ]

        ];


        $data = json_decode($request_handler->post($search['method'], $search['parameters']));

        if(isset($data->display) &&
            $data->display != '' &&
            $data->display != 'Category not found' &&
            ! str_contains($data->display,'longitude_center')){

            echo $data->display;

        }elseif($data->display == 'Category not found'){

            echo '<span class="badge bg-warning d-flex align-items-center py-2 px-3 rounded-pill" role="warning">
            <i class="fa-solid fa-bell fa-shake me-3 fs-5"></i>'.esc_html__('Please select a valid category!', 'cm-marketplace').'</span>';

        }elseif(str_contains($data->display,'longitude_center')){

            echo '<span class="badge bg-info d-flex align-items-center py-4 px-5 rounded-pill" role="warning">
            <i class="fa-solid fa-face-raised-eyebrow fa-bounce me-5 fs-2"></i>'.esc_html__('We didn\'t find anytning here...', 'cm-marketplace').'</span>';

        }

    }

    protected function content_template() {


    }


}
