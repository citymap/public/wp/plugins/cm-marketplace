<?php
namespace Citymap\Widget\Marketplace;

/**
 * Define the internationalization functionality
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @link       https://city-map.com
 * @since      1.0.0
 *
 * @package    Cm_Marketplace
 * @subpackage Cm_Marketplace/lib
 */

/**
 * Define the internationalization functionality.
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @since      1.0.0
 * @package    Cm_Marketplace
 * @subpackage Cm_Marketplace/lib
 * @author     Flexbone GmbH<info@flexbone.systems>
 */
class i18n {


	/**
	 * Load the plugin text domain for translation.
	 *
	 * @since    1.0.0
	 */
	public function load_plugin_textdomain() {

		load_plugin_textdomain(
			'cm-marketplace',
			false,
			dirname( dirname( plugin_basename( __FILE__ ) ) ) . '/languages/'
		);

	}



}
