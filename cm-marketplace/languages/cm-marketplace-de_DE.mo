��    @        Y         �     �     �     �     �     �     �     �     �               (     >     Q     b     |     �     �     �     �     �  #   �     �  `       w     �     	     	     1	     L	  
   ]	  
   h	     s	     |	     �	     �	     �	     �	     �	     �	  ?   �	     

     *
  
   1
     <
     S
  4   \
  ?   �
     �
     �
  
   �
     �
          $     8     <     I     R  ]   n     �     �     �            �  1          &     D     W     u     �     �  
   �  
   �     �     �     �     �          )     C     ]  	   k     u     �  )   �     �  �  �  �   k            ,     1   C     u  	   �     �     �     �     �     �     �     �     �        R     .   Z     �     �     �  
   �  ?   �  >        B      I  
   j  	   u  !        �     �     �     �     �  e   �     Q     ]     x     �     �                  "   $   5   .                  %   ;   ?       #   4                 ,   '   9              3      (   &   6   -               2       7             )       :                    @           8   
            /       >                                               +   *   =   1   !         0   	   <              API Private Key: API Public Key: Activation token: Add pagination on top Address items Business Directory Card content Company name Distance Enter URL of polygon file Enter organization ID Enter radius in Km Entries per page Extended card height (PX) Extended card width Filter by Organization Flexbone GmbH General General Settings Geo polygon Hotels, Restarurants, Lawyers, etc. Icons and heading color In order to use this plugin in Elementor, you will need to add both <strong>public and private API-Keys</strong> linked to your city-map customer number in the form below.</p>
    <p>Your keys can be found in the city-map <a href="https://cmpowersite.com/de/login" target="_blank">customer service center</a> or by contacting your city-map partner.</p> In order to use this plugin in Elementor, you will need to add the activation token provided after you bought this product.</p> Industry Sector Industry name Input your private key here Input your public key here Invalid API-Keys Kilometers List Width Location Maximum entries per page Meters Method Miles More Info Button No Organization Please activate your plugin with a valid token before using it! Please select a valid category! Search Select one Short info height (PX) Subtitle The following API-Keys are registered for this site: The following product token has been registered for this plugin Token: Town / State / Coordinates Typography Units We didn't find anytning here... XXXX-XXXX-XXXX-XXXX Yes businessCARD city-map city-map Business Directory city-map business directory plugin for displaying business cards within an Elementor WP site. companyCARD companyCARD height (PX) companyCARD width https://city-map.com/ https://flexbone.systems Project-Id-Version: city-map Business Directory
PO-Revision-Date: 2022-03-10 18:16+0100
Last-Translator: 
Language-Team: 
Language: de_DE
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Poedit 3.0.1
X-Poedit-Basepath: ..
X-Poedit-Flags-xgettext: --add-comments=translators:
X-Poedit-WPHeader: cm-marketplace.php
X-Poedit-SourceCharset: UTF-8
X-Poedit-KeywordsList: __;_e;_n:1,2;_x:1,2c;_ex:1,2c;_nx:4c,1,2;esc_attr__;esc_attr_e;esc_attr_x:1,2c;esc_html__;esc_html_e;esc_html_x:1,2c;_n_noop:1,2;_nx_noop:3c,1,2;__ngettext_noop:1,2
X-Poedit-SearchPath-0: .
X-Poedit-SearchPathExcluded-0: *.min.js
X-Poedit-SearchPathExcluded-1: vendor
 API Privater Schlüssel: API Öffentlicher Schlüssel: Aktivierungstoken: Paginierung oben in der Liste Adresselemente Branchenverzeichnis Inhalt Firmenname Entfernung URL der Polygondatei eingeben Verein-ID eingeben Radius in Km eingeben Einträge pro Seite Höhe für businessCARDs Breite für businessCARDs Nach Organisation filtern Flexbone GmbH Allgemein Allgemeine Einstellungen Geo-Polygon Hotels, Restaurants, Rechtsanwälte, etc. Symbole und Überschriftenfarbe Um dieses Plugin in Elementor verwenden zu können, müssen Sie sowohl öffentliche als auch <strong>private API-Schlüssel</strong>, die mit Ihrer city-map-Kundennummer verknüpft sind, im folgenden Formular hinzufügen.</p>
    <p>Ihre Schlüssel finden Sie im city-map <a href="https://cmpowersite.com/de/login" target="_blank">KSC</a> oder wenden Sie sich an Ihren city-map Partner.</p> Um dieses Plugin in Elementor zu verwenden, müssen Sie das Aktivierungstoken hinzufügen, das Sie nach dem Kauf dieses Produkts bereitgestellt haben.</p> Branche Branche Geben Sie hier Ihren privaten Schlüssel ein Geben Sie hier Ihren öffentlichen Schlüssel ein Ungültige API-Schlüssel Kilometer Breite der Listenansicht Standort Max. Einträgen pro Seite Meter Methode Meilen Mehr Info Button Nein Verein Bitte aktivieren Sie Ihr Plugin mit einem gültigen Token, bevor Sie es verwenden! Bitte wählen Sie eine gültige Kategorie aus! Suche Eines auswählen Höhe der KurzInfo (PX) Untertitel Die folgenden API-Schlüssel sind für diese Seite registriert: Die folgenden API-Schlüssel sind für diese Seite registriert Token: Ort, Bundesland oder Koordinaten Typografie Einheiten Wir haben hier nichts gefunden... XXXX-XXXX-XXXX-XXXX Ja businessCARD city-map city-map Branchenverzeichnis city-map Brnachenbuch-Plugin zur Anzeige von Branchenverzeichnissen innerhalb eine Elementor WP-Site. companyCARD Höhe der companyCARD (PX) Breite der companyCARD https://city-map.com/ https://flexbone.systems 