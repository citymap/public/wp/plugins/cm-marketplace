<?php

namespace Citymap\Widget\Marketplace;

/**
 * The admin-specific functionality of the plugin.
 *
 * @link       https://city-map.com
 * @since      1.0.0
 *
 * @package    Cm_Marketplace
 * @subpackage Cm_Marketplace/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Cm_Marketplace
 * @subpackage Cm_Marketplace/admin
 * @author     Flexbone GmbH<info@flexbone.systems>
 *
 */

use CityMap\Component\Request\Handler;

class Admin {

	const MAIN_MENU_NAME = 'city-map';

	const CM_API_HOST = "https://my.cmpowersite.com/api/";

	const OPTIONS_GROUP_PREFIX = 'cm_marketplace_';

	const ACTIVE_PLUGIN = 'cm_marketplace_active';

	const SETTINGS_URL 	= 'cm-marketplace-settings';

	const PUBLIC_API_KEY_OPTION = 'cm_api_public_key';

	const PRIVATE_API_KEY_OPTION = 'cm_api_private_key';

	const OPTIONS_PAGE_TITLE = 'city-map Business Directory';

	const OPTIONS_PAGE_MENU = 'city-map';

	const PAGE_ID = 'city-map';

    const PRODUCT_ACTIVATION_TOKEN = 'cm_marketplace_product_token';

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $cm_marketplace    The ID of this plugin.
	 */
	private $cm_marketplace;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $cm_marketplace       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $cm_marketplace, $version ) {

		$this->cm_marketplace = $cm_marketplace;
		$this->version = $version;

	}

	/**
	 * Register the stylesheets for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Cm_Marketplace_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Cm_Marketplace_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->cm_marketplace, plugin_dir_url( __FILE__ ) . 'css/cm-marketplace-admin.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Cm_Marketplace_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Cm_Marketplace_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_script( $this->cm_marketplace, plugin_dir_url( __FILE__ ) . 'js/cm-marketplace-admin.js', array( 'jquery' ), $this->version, false );

	}

	public function init(){

		if ( get_option( self::ACTIVE_PLUGIN ) ) {

			delete_option( self::ACTIVE_PLUGIN );

			if ( ! headers_sent() ) {

				exit( wp_redirect( admin_url( 'admin.php?page='.Admin::SETTINGS_URL)));

			}

		}

	}


	public function registerSettingsOptions(){

        // Group for general settings
        register_setting('cm_general_options_group', self::PUBLIC_API_KEY_OPTION );

        register_setting('cm_general_options_group', self::PRIVATE_API_KEY_OPTION );

        // Group for settings in this plugin
        register_setting(self::OPTIONS_GROUP_PREFIX.'options_group', self::PRODUCT_ACTIVATION_TOKEN );

	}

	public static function registerTopSettingsPage(){

		if(self::hasValidApiKey()){

			require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/partials/Display.php';

		}else{

			require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/partials/InputKeys.php';

		}

	}

	public static function registerSettingsTemplate() {

		if(self::hasValidApiKey() && self::hasValidProductToken()){

			require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/partials/DisplayToken.php';

		}else{

			require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/partials/InputToken.php';

		}

	}

	// checks if the main configuration page for all city-map widgets exists
	public function mainPageExists(){

		global $menu;

		foreach($menu as $item) {

			if(strtolower($item[0]) == strtolower('city-map')) {

				return true;

			}

		}

		return false;

	}


    /**
     * Change Submenu First Item Label
     *
     * Overwrite the label of the first submenu item of an admin menu item.
     *
     * Fired by `admin_menu` action.
     *
     * @since 3.1.0
     *
     * @param $menu_slug
     * @param $new_label
     * @access public
     */
    public static function updateSubmenuFirstItem( $menu_slug, $new_label ) {

        global $submenu;

        if ( isset( $submenu[ $menu_slug ] ) ) {
            // @codingStandardsIgnoreStart
            $submenu[ $menu_slug ][0][0] = $new_label;
            // @codingStandardsIgnoreEnd
        }

    }

    /**
     * Reorder menu items in admin.
     *
     * @param array $menu_order Menu order.
     * @return array
     */
    public function menu_order( $menu_order ) {

        // Initialize our custom order array.
        $cm_menu_order = [];

        // Get the index of our custom separator.
        $separator_cm = array_search( 'separator-cm', $menu_order, true );

        // Get index of plugin menu.
        $menu_index = array_search( self::SETTINGS_URL, $menu_order, true );

        // Loop through menu order and do some rearranging.
        foreach ( $menu_order as $index => $item ) {

            if ( self::PAGE_ID === $item ) {

                $cm_menu_order[] = 'separator-cm';

                $cm_menu_order[] = $item;

                $cm_menu_order[] = self::SETTINGS_URL;

                unset( $menu_order[ $separator_cm ] );

                unset( $menu_order[ $menu_index ] );

            } elseif ( ! in_array( $item, [ 'separator-cm' ], true ) ) {

                $cm_menu_order[] = $item;

            }

        }

        // Return order.
        return $cm_menu_order;
    }

	/*
	*	Wrapper for updateSubmenuFirstItem
	*/
	public function firstSubmenuOverride(){

		$this->updateSubmenuFirstItem(self::PAGE_ID, esc_html__('General Settings','cm-marketplace'));

	}


	public function registerSettingsPage(){

		if( ! $this->mainPageExists() ){

            global $menu;

			// phpcs:ignore WordPress.WP.GlobalVariablesOverride.Prohibited
            $menu[] = [ '', 'read', 'separator-cm', '', 'wp-menu-separator cm' ];

			$cm_icon_base64 = "PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0idXRmLTgiPz4KPCEtLSBHZW5lcmF0b3I6IEFkb2JlIElsbHVzdHJhdG9yIDI2LjAuMywgU1ZHIEV4cG9ydCBQbHVnLUluIC4gU1ZHIFZlcnNpb246IDYuMDAgQnVpbGQgMCkgIC0tPgo8c3ZnIHZlcnNpb249IjEuMSIgaWQ9IkViZW5lXzEiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgeG1sbnM6eGxpbms9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiIHg9IjBweCIgeT0iMHB4IgoJIHZpZXdCb3g9IjAgMCAzMDAgMzAwIiBzdHlsZT0iZW5hYmxlLWJhY2tncm91bmQ6bmV3IDAgMCAzMDAgMzAwOyIgeG1sOnNwYWNlPSJwcmVzZXJ2ZSI+CjxzdHlsZSB0eXBlPSJ0ZXh0L2NzcyI+Cgkuc3Qwe2ZpbGw6IzMwMkUyRTt9Cgkuc3Qxe2ZpbGw6IzMwMkUyRTtzdHJva2U6I0UyRTJFMjtzdHJva2UtbWl0ZXJsaW1pdDoxMDt9Cjwvc3R5bGU+CjxnPgoJPHBhdGggY2xhc3M9InN0MCIgZD0iTTE1Mi4xLDEyMS40bDMzLjIsMy41Yy0zLjYsMTcuOS0xMi45LDMxLjktMjcuOCw0MmMtMTQuOSwxMC4xLTMzLjIsMTUuMi01NC45LDE1LjJjLTI3LjIsMC00OS4xLTctNjUuNi0yMQoJCWMtMTYuNS0xNC0yNC44LTM0LTI0LjgtNjAuMWMwLTE2LjgsMy42LTMxLjYsMTAuNy00NC4zYzcuMS0xMi42LDE4LTIyLjEsMzIuNS0yOC41YzE0LjYtNi4zLDMwLjQtOS41LDQ3LjUtOS41CgkJYzIxLjYsMCwzOS4zLDQuMyw1MywxMy4xYzEzLjcsOC43LDIyLjYsMjEuMSwyNi40LDM3LjFsLTMyLjYsMy41Yy0zLjEtMTAuNi04LjctMTguNi0xNi43LTI0Yy04LTUuNC0xNy42LTgtMjguOS04CgkJYy0xNy4xLDAtMzAuOSw0LjgtNDEuNiwxNC40Yy0xMC43LDkuNi0xNiwyNC44LTE2LDQ1LjVjMCwyMSw1LjEsMzYuMywxNS40LDQ1LjljMTAuMiw5LjUsMjMuNiwxNC4zLDQwLjEsMTQuMwoJCWMxMy4yLDAsMjQuMy0zLjIsMzMuMi05LjZDMTQ0LjEsMTQ0LjUsMTQ5LjcsMTM0LjYsMTUyLjEsMTIxLjQiLz4KCTxwYXRoIGNsYXNzPSJzdDEiIGQ9Ik0yMzUuNiw5Ni43Yy04LjgsMS40LTE4LjUsMTEuOS0yNC4zLDc4LjhjLTAuNyw3LjctMS4yLDE1LjQtMS43LDIyLjhjLTEuMS00LjYtMi4zLTkuMy0zLjUtMTQuMQoJCWMtMTQuNi01Ny4yLTIzLjgtNjcuNi0zMC44LTcwLjJjLTMuNS0xLjMtNy4yLTAuNi0xMC4yLDEuOGMtMTIuNiwxMC4yLTE4LjYsNjUuMS0yMS40LDEwNy45Yy04LjktMzcuMy0yMC04NC4xLTIwLTg0LjFsLTExLjksMi44CgkJbDI5LjksMTI1LjlsMTIuMS0xLjJjMS45LTYwLjcsOS40LTEzMC4yLDE4LjItMTQwLjljOC40LDcuNSwyNC40LDYxLjYsMzUuNywxMjEuNmwxMi4xLTEuMWMwLjktNjkuNiw5LTEzMi4yLDE3LjYtMTM3LjkKCQljOC45LDMuNywyNi4zLDU5LjMsMzcuNCwxMjAuMmwxMi0yLjJjLTAuMS0wLjMtNi0zMi44LTE0LjUtNjQuNEMyNTYuMywxMDEuOCwyNDQuOCw5NS4yLDIzNS42LDk2LjciLz4KPC9nPgo8L3N2Zz4K";

            // Add main settings page
			add_menu_page(
                self::MAIN_MENU_NAME,
                self::MAIN_MENU_NAME,
                'manage_options',
                self::PAGE_ID,
                'Citymap\Widget\Marketplace\Admin::registerTopSettingsPage',
                'data:image/svg+xml;base64,' .$cm_icon_base64,
                40
            );

        }

	}

    public function addPluginSettingsPage(){

		// Add plugin config page
		add_submenu_page(
					 'city-map',
					 str_replace('city-map','',self::OPTIONS_PAGE_TITLE),
					 str_replace('city-map','',self::OPTIONS_PAGE_TITLE),
					 'manage_options',
					  self::SETTINGS_URL,
					 'Citymap\Widget\Marketplace\Admin::registerSettingsTemplate',

		);

    }

	public static function hasValidApiKey(){

		if(get_option(self::PUBLIC_API_KEY_OPTION) != '' && get_option(self::PRIVATE_API_KEY_OPTION) != ''){

			$credentials = ['api' => [
									'response_type' => 'json',
									'host' => self::CM_API_HOST,
									'public_key' => get_option(self::PUBLIC_API_KEY_OPTION),
									'private_key' => get_option(self::PRIVATE_API_KEY_OPTION)
								]
							];

			$parameters = [ 'keys' => 'verify'];

			$request_handler = Handler::getInstance($credentials);

			$response = '['.$request_handler->post('cmps.api.verifyKeys', $parameters).']';

			$response = json_decode($response);

			$response = $response[0];

			if(isset($response->message) && $response->message == 'success'){

				return true;

			}
			if(isset($response->invalid_request)){

				add_settings_error( 'verify-keys', self::PUBLIC_API_KEY_OPTION, esc_html__('Invalid API-Keys','cm-marketplace'));

			}

			return false;

		}

		return false;


	}

	public static function hasValidProductToken(){

		if(get_option(self::PRODUCT_ACTIVATION_TOKEN) != ''){

			return true;

		}

		return false;


	}

}
