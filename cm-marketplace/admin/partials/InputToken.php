<?php

require_once plugin_dir_path( __FILE__ ).'Header.php';

use CityMap\Widget\Marketplace\Admin;

?>
<div class="cm-admin-container">
    <div id="notice-box">
        <?php settings_errors('verify-token');?>
    </div>
    <p><?php _e('In order to use this plugin in Elementor, you will need to add the activation token provided after you bought this product.</p>','cm-marketplace'); ?>
    <form method="post" action="options.php">
        <?php echo settings_fields(Admin::OPTIONS_GROUP_PREFIX.'options_group'); ?>
        <table class="form-table">
            <tr>
                <td><label for="first_field_id"><?php _e('Activation token:','cm-marketplace');?></label></td>
                <td>
    <input type = 'text' placeholder="<?php _e('XXXX-XXXX-XXXX-XXXX','cm-marketplace');?>" class="regular-text" id="<?php echo Admin::PRODUCT_ACTIVATION_TOKEN;?>" name="<?php echo Admin::PRODUCT_ACTIVATION_TOKEN;?>" value="">
                </td>
            </tr>
        </table>
    <?php submit_button(); ?>
</div>
<?php

require_once plugin_dir_path( __FILE__ ).'Notices.php';

?>
