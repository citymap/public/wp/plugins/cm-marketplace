<?php

require_once plugin_dir_path( __FILE__ ).'Header.php';

use CityMap\Widget\Marketplace\Admin;

$obfuscator = '&hellip;**********';

$show_partial = 16;

?>

<div class="cm-admin-container">
    <div id="notice-box"></div>
	<p><?php _e('The following API-Keys are registered for this site:','cm-marketplace');?></p>
    <table class="form-table">
        <tr>
            <td><strong><?php _e('API Public Key:','cm-marketplace');?></strong></td>
            <td><?php echo substr(get_option(Admin::PUBLIC_API_KEY_OPTION), 0, $show_partial).$obfuscator;?></td>
        </tr>
        <tr>
            <td><strong><?php _e('API Private Key:','cm-marketplace');?></strong></td>
            <td><?php echo substr(get_option(Admin::PRIVATE_API_KEY_OPTION), 0, $show_partial).$obfuscator;?></td>
        </tr>
    </table>
</div>
<?php

require_once plugin_dir_path( __FILE__ ).'Notices.php';

?>
