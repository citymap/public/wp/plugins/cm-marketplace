<?php

require_once plugin_dir_path( __FILE__ ).'Header.php';

use CityMap\Widget\Marketplace\Admin;

?>
<div class="cm-admin-container">
    <div id="notice-box">
        <?php settings_errors('verify-keys');?>
    </div>
    <p><?php _e('In order to use this plugin in Elementor, you will need to add both <strong>public and private API-Keys</strong> linked to your city-map customer number in the form below.</p>
    <p>Your keys can be found in the city-map <a href="https://cmpowersite.com/de/login" target="_blank">customer service center</a> or by contacting your city-map partner.</p>','cm-marketplace'); ?>
    <form method="post" action="options.php">
        <?php echo settings_fields('cm_general_options_group'); ?>
        <table class="form-table">
            <tr>
                <td><label for="first_field_id"><?php _e('API Public Key:','cm-marketplace');?></label></td>
                <td>
    <input type = 'text' placeholder="<?php _e('Input your public key here','cm-marketplace');?>" class="regular-text" id="<?php echo Admin::PUBLIC_API_KEY_OPTION;?>" name="<?php echo Admin::PUBLIC_API_KEY_OPTION;?>" value="">
                </td>
            </tr>
            <tr>
                <td><label for="first_field_id"><?php _e('API Private Key:','cm-marketplace');?></label></td>
                <td>
    <input type = 'text' placeholder="<?php _e('Input your private key here','cm-marketplace');?>" class="regular-text" id="<?php echo Admin::PRIVATE_API_KEY_OPTION;?>" name="<?php echo Admin::PRIVATE_API_KEY_OPTION;?>" value="">
                </td>
            </tr>
        </table>
    <?php submit_button(); ?>
</div>
<?php

require_once plugin_dir_path( __FILE__ ).'Notices.php';

?>
