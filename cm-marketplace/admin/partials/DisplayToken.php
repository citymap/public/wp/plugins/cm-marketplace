<?php

require_once plugin_dir_path( __FILE__ ).'Header.php';

use CityMap\Widget\Marketplace\Admin;

$obfuscator = '&hellip;**********';

$show_partial = 16;

?>

<div class="cm-admin-container">	
	<div id="notice-box"></div>
	<p><?php _e('The following product token has been registered for this plugin','cm-marketplace');?></p>
    <table class="form-table">
        <tr>
            <td><strong><?php _e('Token:','cm-marketplace');?></strong></td>
            <td><?php echo substr(get_option(Admin::PRODUCT_ACTIVATION_TOKEN), 0, $show_partial).$obfuscator;?></td>
        </tr>
    </table>
</div>
<?php

require_once plugin_dir_path( __FILE__ ).'Notices.php';

?>
