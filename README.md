# city-map Business Directory

Author: [Patricio Guerrero](https://patricioguerrero.com) | [Flexbone GmbH](https://flexbone.systems) | <info@flexbone.systems>

License: MIT

## Description

city-map business directory plugin for displaying business directories within an Elementor WP site.


## Requirements

 - PHP 7.4.27+
 - Wordpress 5.9
 - Elementor 3.5.5
 - Public and Private API Keys of a city-map customer
 - Product activation token for this plugin

## Dependencies

For **development**, all dependencies must be installed via composer:

```
composer update
```

 - symfony/yaml 5.0+
 - citymap/authenticator 1.2.1+
 - citymap/request 1.0.0+
 - symfony/string 5.0+

### DEV
 - symfony/var-dumper 5.0+

## Usage

 - [Click here](https://gitlab.com/citymap/public/wp/plugins/cm-marketplace/-/blob/develop/_dist/cm-marketplace-latest.zip) and download the latest version of this widget
 - Upload plugin in your Wordpress site
 - Install and activate it using your private and public API keys and the activation token you received when you bought a license for this plugin.
